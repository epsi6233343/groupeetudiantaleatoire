import random

def read_student_data(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()
        student_data = [line.strip().split(';') for line in lines]
    return student_data[1:]

def create_random_groups(student_data, group_size=2):
    random.shuffle(student_data)
    groups = [student_data[i:i + group_size] for i in range(0, len(student_data), group_size)]
    return groups

def save_groups_to_file(groups, output_file):
    with open(output_file, 'w') as file:
        file.write('Groupe N;Prénom;NOM\n')
        for i, group in enumerate(groups, 1):
            for student in group:
                file.write(f'{i};{student[1]};{student[0]}\n')

def main():
    file_path = 'promotion_B3_B.csv'  
    output_file = 'groupes.csv'  

    student_data = read_student_data(file_path)
    groups = create_random_groups(student_data)
    save_groups_to_file(groups, output_file)

if __name__ == '__main__':
    main()
